import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ListService {

  API_URI = 'https://jsonplaceholder.typicode.com/users';

  constructor(private http: HttpClient) { }

  getUsers(){
    return this.http.get(`${this.API_URI}`);
  }

  getUser(id: string){
    return this.http.get(`${this.API_URI}/${id}`);
  }


}
