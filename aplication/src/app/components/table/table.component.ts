import { Component, OnInit } from '@angular/core';
import { ListService } from '../../services/list.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  users:any = [];
  filteredUsers: any = [];
  view = "list";

  constructor(private listServices: ListService) { }

  ngOnInit(): void {
    var storedView = localStorage.getItem('view');
    if(storedView == 'list' || storedView == 'cards'){
        this.view = storedView;
    }

    this.listServices.getUsers().subscribe(
      res => {
        this.users = res;
        this.filteredUsers = this.users
      },
      err => console.log(err)
    );
  }

  storeOption(view : string) {
    localStorage.setItem('view',view);
  }

  searchUsers(event : any){
    var searchTerm = event.target.value
    this.filteredUsers = this.users.filter((user : any) => {
      if(searchTerm != null && searchTerm != '')
        return user.name.toLowerCase().includes(searchTerm.toLowerCase()) || user.address.city.toLowerCase().includes(searchTerm.toLowerCase())  || user.email.toLowerCase().includes(searchTerm.toLowerCase());
      else
        return true;
    })
  }
  
}
