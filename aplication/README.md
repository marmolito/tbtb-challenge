## Requerimientos

para instalar el proytecto debe contar con:

- Node version 16.8.0
- Angular CLI: 12.2.10
- Typescript 4.3.5

## Como instalar el proyecto
Una vez clonado el proyecto debe ubicarse en la raiz de este y ejecutar el comando `npm install` para descargar los archivos de node.

## Como ejecutar el proyecto
Una vez instalados los archivos de node debe ejecutar el comando `ng serve -o` el cual abrirá el proyecto en una pestaña de su navegador.
