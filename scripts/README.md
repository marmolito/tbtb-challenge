## Requerimientos

para instalar la base de datos debe contar con:
- Mysql 5.1.1 o superior

## Como instalar la base de datos

Proceso de instalación para la base de datos **`students_db`** en MySql usando un apache Xampp con administrador de base de datos phpmyadmin.

Una vez descargados los archivos **`db-creation.txt`** y el archivo **`db_implementation.txt`**,
nos dirijimos al phpMyAdmin para realizar la importación.

Primero, vamos la pestaña **importar** que queda en la parte superior central.
Una vez dentro, en el apartado de **importar archivo** damos click en **Seleccionar archivo**.
Se abrira un explorador de archivos donde buscaremos el archivo **`db-creation.txt`** en la 
Ubicación donde lo hayamos descargado.

Para finalizar la importación, damos click en **continuar** en la parte inferior izquierda.

Repetimos este proceso con el archivo **`db_implementation.txt`** el cual contiene todas las creaciones
de los registros y consultas.
