const express  = require('express')
const {route} = require("express/lib/router");
const routes = express.Router()

routes.get('/students',(req, res)=>{
   req.getConnection((err,conn)=>{
       if (err) return res.send(err)

       conn.query('SELECT * FROM students', (err,rows)=>{
           if (err) return res.send(err)

           res.json(rows)
       })
   })
})

routes.get('/students/:id',(req, res)=>{
    req.getConnection((err,conn)=>{
        if (err) return res.send(err)
 
        conn.query('SELECT * FROM students where id = ? limit 1',[req.params.id], (err,rows)=>{
            if (err) return res.send(err)
 
            res.json(rows)
        })
    })
 })

routes.post('/students/store',(req, res)=>{
    req.getConnection((err,conn)=>{
        if (err) return res.send(err)
 
        conn.query('INSERT INTO students set ?',[req.body], (err,rows)=>{
            if (err) return res.send(err)
 
            res.json('student has been inserted')
        })
    })
 })

 routes.delete('/students/:id/delete',(req, res)=>{
    req.getConnection((err,conn)=>{
        if (err) return res.send(err)
 
        conn.query('DELETE FROM students where id = ?',[req.params.id], (err,rows)=>{
            if (err) return res.send(err)
 
            res.send('student has been deleted')
        })
    })
 })

 routes.put('/students/:id/update',(req, res)=>{
    req.getConnection((err,conn)=>{
        if (err) return res.send(err)
 
        conn.query('UPDATE students set ? where id= ?',[req.body, req.params.id], (err,rows)=>{
            if (err) return res.send(err)
 
            res.send('student has been updated')
        })
    })
 })

module.exports = routes