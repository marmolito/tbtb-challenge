## Requisitos

Para instalar el proyecto debe contar con:
- Node version 16.8.0

## Como instalar el proyecto
Una vez clonado el proyecto debe ubicarse en la raiz de este y ejecutar el comando `npm install` para descargar los 
archivos de node

## Como ejecutar el proyecto
Una vez instalados los archivos de node debe ejecutar el comando `ng start` el cual abrirá el proyecto en una 
pestaña de su navegador

## Como probar los servicios

Abrimos **Postman** y nos dirigimos al apartado **collections**, damos click en importar, luego en **folder**,
seleccionamos **chose folder from computer**. Se abrira un explorador de archivos donde seleccionaremos la **Collección**
**`students.postman_collection.json`** finalizamos dando click en **import**.

Luego nos dirigimos **Environment**, damos click en importar, luego en **folder**,
seleccionamos **chose folder from computer**. Se abrira un explorador de archivos donde seleccionaremos el **Environment**
**`students.postman_environment.json`** finalizamos dando click en **import**.

Para finalizar, en la parte superior izquierda encontramos el **listado de Environments disponibles** seleccionaremos 
el environment **`students`**

Despues de esto, ya podemos ejecutar correctamente los 
**Endpoints** disponibles en el **collection** **`students`**